<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registration(){
        return view('signUp');
    }

    public function includeName(request $request){
        
        // dd($request->all());
        $nama1 = $request ["fn"];
        $nama2 = $request ["ln"];

        return view('success', compact('nama1', 'nama2'));
    }

    public function table(){
        return view('adminlte.parts.table');
    }

    public function dataTable(){
        return view('adminlte.parts.dataTable');
    }
}
